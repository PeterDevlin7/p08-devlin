package pdevlin1.keep_away;

import android.graphics.Point;

/**
 * Created by Peter on 5/9/2017.
 */

public class Const {
    static final int MAX_FPS = 60;
    static int SCREEN_WIDTH;
    static int SCREEN_HEIGHT;
    static int SLIDER_RADIUS;
    static int SCREEN_CENTER_X;
    static int SCREEN_CENTER_Y;
    static int X_MIN;
    static int X_MAX;
    static int Y_MIN;
    static int Y_MAX;
    static long START_TIME;
    static boolean GAME_OVER = false;
}
