package pdevlin1.keep_away;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.SystemClock;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;

/**
 * Created by Peter on 5/9/2017.
 */

public class GameView extends SurfaceView implements SurfaceHolder.Callback {
    private MainThread thread;
    public Target target;
    public ArrayList<Flyer> flyers;
    public Slider slider;
    private int numFlyers = 0;
    private int ticks = 0;
    private int tickMax = 500;
    private Paint scorePaint;
    private int score = 0;

    public GameView(Context context) {
        super(context);
        getHolder().addCallback(this);
        thread = new MainThread(getHolder(), this);

        target = new Target();
        flyers = new ArrayList<>();
        flyers.add(new Flyer(++numFlyers));
        slider = new Slider();
        scorePaint = new Paint();
        scorePaint.setTextSize(90);
        scorePaint.setFakeBoldText(true);
        scorePaint.setTextAlign(Paint.Align.CENTER);
        scorePaint.setColor(Color.GRAY);
        Const.START_TIME = SystemClock.elapsedRealtime();
        setFocusable(true);
    }

    public void update() {
        if(!Const.GAME_OVER) {
            for(Flyer f : flyers) {
                f.update(flyers);
            }
            target.update(flyers);
            slider.update(flyers);
            if(numFlyers < 12) {
                ticks++;
                if (ticks >= tickMax) {
                    flyers.add(new Flyer(++numFlyers));
                    ticks = 0;
                    tickMax *= 1.2;
                }
            }
        }
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if(!Const.GAME_OVER){
            canvas.drawColor(Color.LTGRAY);
            score = (int)(SystemClock.elapsedRealtime() - Const.START_TIME)/100;
            canvas.drawText(String.valueOf(score), Const.SCREEN_CENTER_X, Const.SCREEN_CENTER_Y + 180, scorePaint);
            canvas.drawLine(0,Const.SCREEN_HEIGHT, Const.SCREEN_WIDTH, Const.SCREEN_HEIGHT, new Paint());
            for(Flyer f : flyers) {
                f.draw(canvas);
            }
            target.draw(canvas);
            slider.draw(canvas);
        }else{
            GameOverScreen gameOverScreen = new GameOverScreen(this);
            gameOverScreen.draw(canvas);
        }
    }

    private void resetGame() {

        flyers.clear();
        Const.GAME_OVER = false;
        numFlyers = 0;
        Const.START_TIME = SystemClock.elapsedRealtime();
        flyers.add(new Flyer(++numFlyers));
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch(event.getAction()) {

            case (MotionEvent.ACTION_DOWN):

                if (Const.GAME_OVER) {
                    resetGame();
                }
                break;

            case (MotionEvent.ACTION_MOVE):

                float x = event.getX();
                float y = event.getY();
                slider.setAngle(x,y);
                break;

        }
        return true;
    }

    int getScore() {
        return score;
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        thread = new MainThread(getHolder(), this);
        thread.setRunning(true);
        thread.start();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        while(retry){
            try {
                thread.setRunning(false);
                thread.join();
            } catch(Exception e) { e.printStackTrace(); }

            retry = false;
        }
    }

}
