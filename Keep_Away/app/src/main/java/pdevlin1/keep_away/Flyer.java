package pdevlin1.keep_away;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Peter on 5/9/2017.
 */

public class Flyer {

    private Point center;
    private int dx;
    private int dy;
    private int radius;
    private Paint paint;
    private int id;
    private int cycle;

    public Flyer(int idIn) {

        /**
         * Set up color and radius and ID
         */
        paint = new Paint();
        paint.setColor(Color.RED);
        radius = Const.SCREEN_WIDTH/24;
        id = idIn;
        cycle = 0;
        /**
         * Set random start coordinates
         */
        Random rng = new Random(id*50);
        int randx = Const.SCREEN_CENTER_X, randy = Const.SCREEN_CENTER_Y;
        while((randx > Const.X_MIN && randx < Const.X_MAX) || (randx < radius) || (randx > Const.SCREEN_WIDTH - radius)) {
            randx = rng.nextInt(Const.SCREEN_WIDTH);
        }
        while((randy > Const.Y_MIN && randy < Const.Y_MAX) || (randy < radius) || (randy > Const.SCREEN_HEIGHT - radius)) {
            randy = rng.nextInt(Const.SCREEN_HEIGHT);
        }
        center = new Point(randx, randy);

        /**
         * Set random vectors for flyer movement
         */
        dx = 4 + rng.nextInt(3);
        dy = 4 + rng.nextInt(3);
        if(Math.random() > 0.5) dx *= -1;
        if(Math.random() > 0.5) dy *= -1;
    }

    Point getCenter() {
        return center;
    }

    void setCenter(Point c) {
        center = c;
    }

    int getID() {
        return id;
    }

    int getDx() {
        return dx;
    }

    void setDx(int dxIn) {
        dx = dxIn;
    }

    int getDy() {
        return dy;
    }

    void setDy(int dyIn) {
        dy = dyIn;
    }

    double getDistance(Point p) {
        int diffX = p.x - center.x;
        int diffY = p.y - center.y;
        return Math.sqrt(diffX*diffX + diffY*diffY);
    }

    boolean collidesWith(Point otherCenter, int radIn) {

        double distance = getDistance(otherCenter);
        if(radIn > radius) {
            return distance <= radIn + radius*.9;
        }
        if(distance < radius * 1.5) {
            center.x += radius*2;
            center.y += radius*2;
        }
        return distance <= radius * 1.9;
    }

    void swapVectors(Flyer f) {

        int tmpDx = f.getDx();
        int tmpDy = f.getDy();
        f.setDx(dx);
        f.setDy(dy);
        dx = tmpDx;
        dy = tmpDy;
    }

    void draw(Canvas canvas) {

        double distance = getDistance(new Point(Const.SCREEN_CENTER_X, Const.SCREEN_CENTER_Y));
        if(distance < Const.SLIDER_RADIUS) {
            if(paint.getColor() == Color.WHITE && cycle == 3) {
                paint.setColor(Color.RED);
            }
            else {
                if(cycle == 3){
                    paint.setColor(Color.WHITE);
                }
            }
        }
        else {
            int red = 255 - (int)(100 * distance/Const.SCREEN_HEIGHT);
            red = red > 0 ? red : 0;
            int green = 255 - (int)(900 * distance/Const.SCREEN_HEIGHT);
            green = green > 0 ? green : 0;
            int blue = 255 - (int)(900 * distance/Const.SCREEN_HEIGHT);
            blue = blue > 0 ? blue : 0;
            paint.setColor(Color.argb(255,red,green,blue));
            //paint.setColor(Color.RED);
        }
        canvas.drawCircle(center.x, center.y, radius, paint);
        cycle++;
        if(cycle > 4) cycle = 0;
    }

    void update(ArrayList<Flyer> flyers) {
        /**
         * Handle boundary bouncing
         */
        if((dx > 0 && center.x > Const.SCREEN_WIDTH - radius) ||
                (dx < 0 && center.x < radius)) dx *= -1;
        if((dy > 0 && center.y > Const.SCREEN_HEIGHT - radius) ||
                (dy < 0 && center.y < radius)) dy *= -1;

        /**
         * Check for collisions with other flyers
         */
        for(Flyer f : flyers) {
            if(f.getID() > id && collidesWith(f.getCenter(),0)) {
                swapVectors(f);
                break;
            }
        }
        center.x += dx;
        center.y += dy;
    }
}
