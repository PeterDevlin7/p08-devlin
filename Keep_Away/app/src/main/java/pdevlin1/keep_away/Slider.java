package pdevlin1.keep_away;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.DashPathEffect;
import android.graphics.Point;
import android.graphics.RectF;
import android.support.constraint.solver.widgets.ConstraintAnchor;

import java.util.ArrayList;
import java.util.Vector;

/**
 * Created by Peter on 5/10/2017.
 */

public class Slider {

    private float arcLength;
    private Point center;
    private Paint d_paint;
    private Paint s_paint;
    private RectF bounds;
    private float startAngle;

    public Slider() {

        arcLength = 90; startAngle = 180;
        center = new Point(Const.SCREEN_CENTER_X, Const.SCREEN_CENTER_Y);
        /**
         * Set the dashed line pattern
         */
        d_paint = new Paint();
        d_paint.setPathEffect(new DashPathEffect(new float[]{10,10}, (float)1.0));
        d_paint.setColor(Color.DKGRAY);
        d_paint.setStyle(Paint.Style.STROKE);
        /**
         * Set the slider pattern
         */
        s_paint = new Paint();
        s_paint.setStyle(Paint.Style.STROKE);
        s_paint.setStrokeCap(Paint.Cap.ROUND);
        s_paint.setStrokeWidth(17);
        s_paint.setColor(Color.GRAY);
        /**
         * Set the bounds of the slider
         */
        bounds = new RectF(Const.SCREEN_CENTER_X - Const.SLIDER_RADIUS,
                Const.SCREEN_CENTER_Y - Const.SLIDER_RADIUS,
                Const.SCREEN_CENTER_X + Const.SLIDER_RADIUS,
                Const.SCREEN_CENTER_Y + Const.SLIDER_RADIUS);
    }

    void setAngle(float xIn, float yIn) {

        startAngle = determineAngle(xIn, yIn);
    }

    float determineAngle(float x, float y) {

        float angle = (float)(360 * Math.atan2(y-Const.SCREEN_CENTER_Y,x-Const.SCREEN_CENTER_X) / (2*Math.PI));
        return angle > 0 ? angle : angle + 360;
    }

    void bounceOut(Flyer f, float flyerAngle) {

        int dx = f.getDx();
        int dy = f.getDy();
        double flyAngleRad = flyerAngle * Math.PI / 180;
        int newDx = (int)(Math.cos(flyAngleRad)*dx);
        int newDy = (int)(Math.sin(flyAngleRad)*dy);
        /**
         * Left side acute
         */
        if(dx > 0 && flyerAngle >= 135 && flyerAngle < 225) {
            f.setDx(newDx);
            newDy = dy > 0 ? dy + dx + newDx : dy - dx - newDx;
            f.setDy(newDy);
            return;
        }
        /**
         * Left side obtuse
         */
        if(dx <= 0 && flyerAngle >= 135 && flyerAngle < 225) {
            f.setDx(dx-Math.abs(newDy));
            newDy = dy > 0 ? dy - Math.abs(newDy) : dy + Math.abs(newDy);
            f.setDy(newDy);
            return;
        }
        /**
         * Right side acute
         */
        if(dx < 0 && (flyerAngle < 45 || flyerAngle >= 315)) {
            f.setDx(-newDx);
            newDy = dy > 0 ? dy - (-newDx + dx) : dy + (-newDx + dx);
            f.setDy(newDy);
            return;
        }
        /**
         * Right side obtuse
         */
        if(dx >= 0 && (flyerAngle < 45 || flyerAngle >= 315)) {
            f.setDx(dx+Math.abs(newDy));
            newDy = dy > 0 ? dy - Math.abs(newDy) : dy + Math.abs(newDy);
            f.setDy(newDy);
            return;
        }
        /**
         * Top side acute
         */
        if(dy > 0 && flyerAngle >= 225 && flyerAngle < 315) {
            f.setDy(newDy);
            newDx = dx > 0 ? dx + (newDy + dy) : dx - (newDy + dy);
            f.setDx(newDx);
            return;
        }
        /**
         * Top side obtuse
         */
        if(dy <= 0 && flyerAngle >= 225 && flyerAngle < 315) {
            f.setDy(dy-Math.abs(newDx));
            newDx = dx > 0 ? dx - Math.abs(newDx) : dx + Math.abs(newDx);
            f.setDx(newDx);
            return;
        }
        /**
         * Bottom side acute
         */
        if(dy < 0 && flyerAngle >= 45 && flyerAngle < 135) {
            f.setDy(-newDy);
            newDx = dx > 0 ? dx - (-newDy + dy) : dx + (-newDy + dy);
            f.setDx(newDx);
            return;
        }
        /**
         * Bottom side obtuse
         */
        if(dy > 0 && flyerAngle >= 45 && flyerAngle < 135) {
            f.setDy(dy+Math.abs(newDx));
            newDx = dx > 0 ? dx - Math.abs(newDx) : dx + Math.abs(newDx);
            f.setDx(newDx);
            return;
        }
    }

    void bounceIn(Flyer f, float flyerAngle) {

        int dx = f.getDx();
        int dy = f.getDy();
        double flyAngleRad = flyerAngle * Math.PI / 180;
        int newDx = (int)(Math.cos(flyAngleRad)*dx);
        int newDy = (int)(Math.sin(flyAngleRad)*dy);
        /**
         * Left side
         */
        if(dx < 0 && flyerAngle >= 135 && flyerAngle < 225) {
            f.setDx(newDx);
            newDy = dy > 0 ? dy - dx - newDx : dy + dx + newDx;
            f.setDy(newDy);
        }
        /**
         * Right side
         */
        if(dx > 0 && (flyerAngle < 45 || flyerAngle >= 315)) {
            f.setDx(-newDx);
            newDy = dy > 0 ? dy + (-newDx + dx) : dy - (-newDx + dx);
            f.setDy(newDy);
        }
        /**
         * Top side
         */
        if(dy < 0 && flyerAngle >= 225 && flyerAngle < 315) {
            f.setDy(newDy);
            newDx = dx > 0 ? dx + (-newDy - dy) : dx - (-newDy - dy);
            f.setDx(newDx);
        }
        /**
         * Bottom side
         */
        if(dy > 0 && flyerAngle >= 45 && flyerAngle < 135) {
            f.setDy(-newDy);
            newDx = dx > 0 ? dx + (-newDy + dy) : dx - (-newDy + dy);
            f.setDx(newDx);
        }
    }

    void draw(Canvas canvas) {

        canvas.drawCircle(center.x, center.y, Const.SLIDER_RADIUS - 7, d_paint);
        canvas.drawArc(bounds, startAngle-(arcLength/2), arcLength, false, s_paint);
    }

    void update(ArrayList<Flyer> flyers) {

        /**
         * Push the flyers away if they bump into the slider
         */
        for(Flyer f : flyers) {
            Point center = f.getCenter();
            float flyerAngle = determineAngle(center.x, center.y);
            float diffX = Const.SCREEN_CENTER_X - center.x;
            float diffY = Const.SCREEN_CENTER_Y - center.y;
            float distanceFromCenter = (float)Math.sqrt(diffX*diffX + diffY*diffY);
            if(distanceFromCenter <= Const.SLIDER_RADIUS*1.25 && distanceFromCenter >= Const.SLIDER_RADIUS*0.9 &&
                    (flyerAngle >= startAngle-(5*arcLength/9) && flyerAngle <= startAngle+(5*arcLength/9))) {
                bounceOut(f, flyerAngle);
            }
            if(distanceFromCenter >= Const.SLIDER_RADIUS*0.8 && distanceFromCenter < Const.SLIDER_RADIUS*0.9 &&
                    (flyerAngle >= startAngle-(5*arcLength/9) && flyerAngle <= startAngle+(5*arcLength/9))) {
                bounceIn(f, flyerAngle);
            }
        }
    }
}
