package pdevlin1.keep_away;

import android.app.Activity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE;
        decorView.setSystemUiVisibility(uiOptions);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        Const.SCREEN_WIDTH = dm.widthPixels;
        Const.SCREEN_HEIGHT = dm.heightPixels;
        Const.SCREEN_CENTER_X = Const.SCREEN_WIDTH/2;
        Const.SCREEN_CENTER_Y = Const.SCREEN_HEIGHT/2;
        Const.SLIDER_RADIUS = Const.SCREEN_WIDTH/5;
        Const.X_MIN = Const.SCREEN_CENTER_X - Const.SLIDER_RADIUS;
        Const.X_MAX = Const.SCREEN_CENTER_X + Const.SLIDER_RADIUS;
        Const.Y_MIN = Const.SCREEN_CENTER_Y - Const.SLIDER_RADIUS;
        Const.Y_MAX = Const.SCREEN_CENTER_Y + Const.SLIDER_RADIUS;

        setContentView(new GameView(this));
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
