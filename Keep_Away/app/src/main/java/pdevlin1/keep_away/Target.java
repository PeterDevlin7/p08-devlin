package pdevlin1.keep_away;

import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Canvas;
import android.graphics.Paint;

import java.util.ArrayList;

/**
 * Created by Peter on 5/9/2017.
 */

public class Target {

    private Point center;
    private int radius;
    private Paint paint;
    private int growth;
    private double growthTick;

    public Target() {

        center = new Point(Const.SCREEN_CENTER_X, Const.SCREEN_CENTER_Y);
        radius = Const.SCREEN_WIDTH/18;
        paint = new Paint();
        paint.setColor(Color.DKGRAY);
        growth = 1;
        growthTick = 0.0;
    }

    void draw(Canvas canvas) {

        canvas.drawCircle(center.x, center.y, radius, paint);
    }

    void update(ArrayList<Flyer> flyers) {

        growthTick += 0.25;
        if(growthTick == 1.0) {
            radius += growth;
            growthTick = 0.0;
        }
        if(radius < Const.SCREEN_WIDTH/20 || radius > Const.SCREEN_WIDTH/16){
            growth *= -1;
            radius += growth;
        }
        for(Flyer f : flyers) {
            if(f.collidesWith(center, radius)) {
                Const.GAME_OVER = true;
                break;
            }
        }
    }
}
