package pdevlin1.keep_away;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

/**
 * Created by Peter on 5/9/2017.
 */

public class GameOverScreen {
    private Paint textPaint;
    private GameView gameView;

    GameOverScreen(GameView gameViewIn) {
        textPaint = new Paint();
        textPaint.setTextAlign(Paint.Align.CENTER);
        textPaint.setTextSize(150);
        textPaint.setFakeBoldText(true);
        textPaint.setColor(Color.WHITE);
        gameView = gameViewIn;
    }

    void draw(Canvas canvas) {
        canvas.drawText("GAME", Const.SCREEN_CENTER_X, Const.SCREEN_CENTER_Y, textPaint);
        canvas.drawText("OVER", Const.SCREEN_CENTER_X, Const.SCREEN_CENTER_Y + 120, textPaint);
        textPaint.setTextSize(100);
        canvas.drawText("Your score was " + gameView.getScore(), Const.SCREEN_CENTER_X, Const.SCREEN_CENTER_Y + 200, textPaint);
        canvas.drawText("Touch to play again", Const.SCREEN_CENTER_X, Const.SCREEN_CENTER_Y+300, textPaint);
    }
}

